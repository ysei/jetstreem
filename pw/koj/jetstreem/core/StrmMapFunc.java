package pw.koj.jetstreem.core;

public interface StrmMapFunc {
    public abstract Object call(Streem strm, Object data);
}

