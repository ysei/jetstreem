package pw.koj.jetstreem.core;

public interface StrmFunc {
    public abstract void call(Streem strm, Object data);
}

