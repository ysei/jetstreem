package pw.koj.jetstreem.ast;

public interface Dumpable {
    void dump(Dumper d);
}

