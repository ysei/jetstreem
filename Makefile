SRCDIR = pw/koj/jetstreem/

all: $(SRCDIR)parser/Parser.class

$(SRCDIR)parser/Parser.class: $(SRCDIR)/parser/Parser.java
	javac $(SRCDIR)parser/Parser.java

core: 
	javac $(SRCDIR)/core/Main.java
